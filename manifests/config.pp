# Configure.
#
class gossinbackup::config {

  $defaults = { 'path' => "${gossinbackup::install_directory}/gossin-backup.conf" }
  $settings = {
    'Linux' => {
      'source'         => $gossinbackup::source_directory,
      'basedest'       => $gossinbackup::destination_directory,
      'exclude'        => $gossinbackup::exclude,
      'days'           => $gossinbackup::days,
      'months'         => $gossinbackup::months,
      'years'          => $gossinbackup::years,
      'max_file_size'  => $gossinbackup::max_file_size,
    }
  }
  create_ini_settings($settings, $defaults)

  cron { 'gossin-backup':
    ensure  => present,
    command => "${gossinbackup::install_directory}/gossin-backup",
    user    => 'root',
    hour    => $gossinbackup::exec_hours,
    minute  => $gossinbackup::exec_minutes,
  }
}
