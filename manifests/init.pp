# Main class.
#
class gossinbackup (
  Stdlib::Absolutepath $source_directory,
  Stdlib::Absolutepath $destination_directory,
  Stdlib::Absolutepath $install_directory,
  Stdlib::Absolutepath $exclude,
  Integer $days,
  Integer $months,
  Integer $years,
  String $max_file_size,
  Tuple $exec_hours,
  Tuple $exec_minutes,
) {
  contain gossinbackup::install
  contain gossinbackup::config
  Class['::gossinbackup::install'] -> Class['::gossinbackup::config']
}
