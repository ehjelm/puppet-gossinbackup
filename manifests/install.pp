# Install class.
#
class gossinbackup::install {

  package { ['git','rsync','crudini']:
    ensure => latest,
  }

  vcsrepo { $gossinbackup::install_directory:
    ensure   => present,
    provider => git,
    source   => 'https://github.com/githubgossin/gossin-backup.git',
    require  => Package['git'],
  }

  file { "${gossinbackup::install_directory}/gossin-backup":
    mode    => '0755',
    require => Vcsrepo[$gossinbackup::install_directory],
  }

}
