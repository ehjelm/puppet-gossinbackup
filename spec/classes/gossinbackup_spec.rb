require 'spec_helper'

describe 'gossinbackup' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          'source_directory'      => '/home',
          'destination_directory' => '/backups'
        }
      end
      it { is_expected.to compile }
    end
  end
end
