require 'spec_helper'

describe 'gossinbackup::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:node_params) { { 'gossinbackup::install_directory' => '/opt' } }
      it { is_expected.to compile }
      it { is_expected.to contain_package('rsync') }
      it { is_expected.to contain_package('crudini') }
      it {
        is_expected.to contain_package('git')
          .that_comes_before('File[/opt/gossin-backup]')
      }
      # it { pp catalogue.resources } # display catalogue, not shown with pdk
    end
  end
end
