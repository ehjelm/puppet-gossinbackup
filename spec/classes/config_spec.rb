require 'spec_helper'

describe 'gossinbackup::config' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:node_params) do
        {
          'gossinbackup::install_directory'     => '/opt/gossin-backup',
          'gossinbackup::source_directory'      => '/home',
          'gossinbackup::destination_directory' => '/backups',
          'gossinbackup::exclude'               => '/opt/rsyncexcludes',
          'gossinbackup::days'                  => 7,
          'gossinbackup::months'                => 12,
          'gossinbackup::years'                 => 3,
          'gossinbackup::max_file_size'         => '100M',
          'gossinbackup::exec_hours'            => ['*'],
          'gossinbackup::exec_minutes'          => [15, 45]
        }
      end
      it { is_expected.to compile }
    end
  end
end
